//
//  ViewController.m
//  TYJWTModule
//
//  Created by 夏伟 on 2016/12/20.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import "ViewController.h"
#import "TYJWTHandler.h"

@interface ViewController ()

@property (nonatomic, copy) NSString *token;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (IBAction)encodeAction:(id)sender {
    self.token = [TYJWTHandler jwtEncodeWithBaseUrl:@"http://192.168.10.10:8080"];
    NSLog(@"Create a JWT with a secret: %@", self.token);
}

- (IBAction)decodeAction:(id)sender {
    NSDictionary *envelopedPayload = [TYJWTHandler jwtDecoder:self.token];
    NSLog(@"Check JWT signature: %@", envelopedPayload);
}
@end
