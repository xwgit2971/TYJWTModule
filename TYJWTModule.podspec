Pod::Spec.new do |s|
  s.name = 'TYJWTModule'
  s.version = '0.0.4'
  s.platform = :ios, '8.0'
  s.license = { type: 'MIT', file: 'LICENSE' }
  s.summary = 'TYJWTModule'
  s.homepage = 'https://gitlab.com/xwgit2971/TYJWTModule'
  s.author = { 'SunnyX' => '1031787148@qq.com' }
  s.source = { :git => 'git@gitlab.com:xwgit2971/TYJWTModule.git', :tag => s.version }
  s.source_files = 'TYJWTModule/*.{h,m}'
  s.resources = 'TYJWTModule/Resources/*.*'
  s.framework = 'Foundation'
  s.requires_arc = true
  s.dependency  'JWT', '~> 3.0.0-beta.1'
end
