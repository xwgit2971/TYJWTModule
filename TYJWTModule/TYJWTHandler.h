//
//  TYJWTHandler.h
//  TYJWTModule
//
//  Created by 夏伟 on 2016/12/21.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TYJWTHandler : NSObject

// Create a JWT with a secret
+ (NSString *)jwtEncodeWithBaseUrl:(NSString *)baseUrl;
// Check JWT signature
+ (NSDictionary *)jwtDecoder:(NSString *)token;

@end
