//
//  TYJWTHandler.m
//  TYJWTModule
//
//  Created by 夏伟 on 2016/12/21.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import "TYJWTHandler.h"
#import <JWT/JWT.h>

@implementation TYJWTHandler

+ (NSString *)jwtEncodeWithBaseUrl:(NSString *)baseUrl {
    // Encode
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"privateKey" ofType:@"p12"];
    NSData *privateKeySecretData = [NSData dataWithContentsOfFile:filePath];

    NSString *passphraseForPrivateKey = @"touyunbusiness";

    NSString *algorithmName = @"RS256";

    JWTClaimsSet *claimsSet = [[JWTClaimsSet alloc] init];
    claimsSet.issuer = @"shop-business-ios-app";
    claimsSet.subject = @"shop-business-ios-app";
    claimsSet.audience = [baseUrl stringByAppendingString:@"/oauth/token"];
    claimsSet.expirationDate = [NSDate distantFuture];
    claimsSet.notBeforeDate = [NSDate distantPast];
    claimsSet.issuedAt = [NSDate date];
    CFUUIDRef uuidref = CFUUIDCreate(kCFAllocatorDefault);
    NSString *uuidStr = (NSString *)CFBridgingRelease(CFUUIDCreateString (kCFAllocatorDefault,uuidref));
    claimsSet.identifier = uuidStr;

    id<JWTAlgorithm> algorithm = [JWTAlgorithmFactory algorithmByName:algorithmName];

    JWTBuilder *builder = [JWTBuilder encodeClaimsSet:claimsSet].secretData(privateKeySecretData).privateKeyCertificatePassphrase(passphraseForPrivateKey).algorithm(algorithm);
    NSString *token = builder.encode;
    // check error
    if (builder.jwtError) {
        // error occurred.
        NSLog(@"Encode: %@", builder.jwtError.localizedDescription);
    }
    return token;
}

+ (NSDictionary *)jwtDecoder:(NSString *)token {
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"publicKey" ofType:@"pem"];

    // Decode
    // Suppose, that you get token from previous example. You need a valid public key for a private key in previous example.
    // Private key stored in @"secret_key.p12". So, you need public key for that private key.
    NSString *publicKey =  [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil]; // load public key. Or use it as raw string.

    NSString *algorithmName = @"RS256";

    JWTBuilder *decodeBuilder = [JWTBuilder decodeMessage:token].secret(publicKey).algorithmName(algorithmName);
    NSDictionary *envelopedPayload = decodeBuilder.decode;
    // check error
    if (decodeBuilder.jwtError) {
        // error occurred.
        NSLog(@"Decode: %@", decodeBuilder.jwtError.localizedDescription);
    }
    return envelopedPayload;
}

@end
